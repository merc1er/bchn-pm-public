# BCHN Scheduling

The TaskJugger (https://taskjuggler.org) tool used to plan the project
and generate scheduling reports.


## Requirements

The project files are processed using TaskJugger (https://taskjuggler.org)
version 3.6.0 or better (package `tj3` on Debian 10.3 or higher).


## Files

Project (.tjp) and report (.tji) definitions are used to generate project
schedule HTML reports (temporarily committed in html/ folder -
TODO: generation and deployment to be moved to GitLab CI).

The `bchnlogo.png` graphic has been added to `html/icons/` to include
the graphical logo in the reports.


## Generating the reports

The command `tj3 bchn-main-project.tjp` should update the `html/` files
unless it reports an error in processing the data.


## Maintenance of the project planning data itself:

The task and report definitions are currently maintained manually via
text editor.

Some instructions are contained within the .tjp and .tji files, with
guidelines to be added to other project documentation in future.
